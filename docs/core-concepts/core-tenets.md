# Core Tenets

The goal of the Common Application Platform is to provide tools and infrastructure that allow application development teams to focus on solving the needs of their stakeholders. While working towards the goal, we strive to keep and abide by the following core tenets:

- **Lower the barrier of entry.** In every place possible, we want to make it as easy as possible for our application teams to provide value to their stakeholders. Examples might include creating CI templates for common tasks, automating log forwarding, or creating Terraform modules to allow apps to assume roles in other AWS accounts.

- **Dogfood our own processes.** Wherever possible, we seek to ["eat our own dog food."](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) The more we use the processes we promote, the more likely we are to find painpoints and identify ways to remove barriers. Beyond required cluster services, all of our applications are deployed as separate tenants on the platform.

- **Listen. Listen. Listen.** As our customers run into problems or need additional capabilities, we will listen and seek to be partners to help them meet their needs. Where possible, we will also seek to generalize solutions so other teams may benefit as well.

- **Everything as code.** All of our infrastructure, pipelines, and application deployments are defined as code. This ensures everything is documented, all configuration is visible to the entire team, and all changes are able to be applied via automated means. In the case of a major disaster, rebuilding will be much easier if everything is code from the start.

- **GitOps all the things.** If everything is defined as code, all changes _must_ be applied only through pipelines or other automated means (including components that pull changes into the cluster). No changes to the production cluster are to be applied via manual means.

- **Lock ourselves out of production.** If all changes are applied via GitOps, we, as platform admins, have no need to have write access to the cluster. By ensuring admins have only read-only access to production machines or services, it's that much harder for nefarious actors to gain it. Gaining write access requires escalation, which is closely monitored and automatically removed within 12 hours.
