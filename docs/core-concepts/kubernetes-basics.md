# Kubernetes Basics

Here are come basic concepts anyone who wishes to use the platform should be
familiar with, and the kubernetes objects which are used to implement those
concepts.

## Pods

Kubernetes runs containers in _pods_, which can run one or more containers
within the same network and storage namespace.  Containers running within the
same pod can therefore access network services on containers within the same pod
by accessing the port on _localhost_ and the storage by a shared filesystem
path.

Applications should be designed to scale horizontally by adding more pods, which
implies the potential instantiation of multiple containers.

## Services and Ingresses

In order to communicate between pods and with the outside world, pod ports
should be mapped to services.  HTTP services can also be exposed outside of the
cluster by creating an Ingress object.

## Certificates

Defining certificate objects will cause the cluster to automatically issue a
certificate from the LetsEncrypt certificate authority.  The certificate will
be stored within your tenant namespace and can be attached to an Ingress object.