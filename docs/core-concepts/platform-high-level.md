
# The 5000ft View of the Platform

The Common Application Platform's hosting environment seeks to make it easy to run containerized workloads. The process works more-or-less as follows:

1. **Gain cluster access.** We're still figuring out what this looks like, as several considerations come into play. Stay tuned to get access!

1. **A platform tenant is created and configured.** During this step, we provision the various resources needed to run your applications, including a dedicated namespace and a _manifest repo_. Additional configuration might include automatic log forwarding to send application logs to your own CLS group and the domain names you are authorized to use.

1. **You define your application manifests.** In the manifest repo, you create manifest files describing your application. Using version control lets you see the history of changes over time and allows you to use a variety of processes to review and apply changes. How you update these manifests are up to you. You can use manual processes or automated pipelines that deploy updates immediately after new container images are built (we have CI templates to help with this!).

1. **The manifests are applied.** Components in the cluster watch this repo and apply the manifests as they change into the tenet's namespace. From there, our cluster components will kick in to make your desired state a reality!

1. **The apps startup and are accessible.** Once the containers start, they are accessible at their configured domains. While we don't want to _own_ the DNS names, you are welcome to create CNAMEs that resolve to our cluster (prod.clusters.aws.platform.it.cloud.vt.edu). Certificates will automatically be provisioned and used for your authorized domains.

1. **Debug your application using kubectl, CLS, or Grafana.** All tenants are able to access the cluster using the native Kubernetes CLI (kubectl) after authentication. We provide read-only access to help troubleshoot deployment issues. When configuring your tenant, we can configure all application logs to be send to a CLS group of your choosing. We can also automatically scrape exposed Prometheus metrics and allow you to create Grafana dashboards and alerting (only requires an extra manifest or two... a how-to guide is coming soon!).

1. **Loop back to step 3.** As you need to make changes to your application to fix bugs or respond to stakeholder needs, simply update your manifests to reference your new container image. Once committed, it'll be deployed!

That's pretty much it! If you want to dive more into how the platform itself works, feel free to go through the [Getting Started tutorial](/getting-started/), [go through our guides](/guides/), or [visit the Architecture Docs](/architecture-docs/) to dive into how the platform itself works!
