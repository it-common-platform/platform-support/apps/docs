
# Separation of Concerns

Just as AWS has its [Shared Responsibility Model](https://aws.amazon.com/compliance/shared-responsibility-model/), we seek to answer the question "who is in charge of what?" This page seeks to provide that answer.

For the most part, the answer can be summarized as follows: **you are responsible for your application, what it does, and how it operates and we are responsible for the underlying infrastructure it runs on and core services it relies on.**

To break it down a little further, the following principles apply, where "we" represents the platform team and "you" represents an application team:

- **We will maintain patching of all machines and core services.** We are responsible for ensuring all machines, container runtimes, and cluster services are updated and maintained.

- **We will ensure proper security controls are in place between tenants.** To prevent a compromise of one application from affecting another, we will implement appropriate barriers between applications. If your application is ever compromised because of a compromise from another team, that's on us.

- **Your app, your choice.** You are welcome to choose any language, framework, or tool. As long as it's a container, we don't care.

- **How you build your container image is up to you.** You are welcome to build your containers however you choose. We'd obviously recommend using CI pipelines (we have CI templates to help you get started), but you're welcome to manually build them if you'd like.

- **You are responsible for maintaining your application.** You are responsible for ensuring your running application is using updated base images, libraries, and source code. We are not responsible for compromises that occur due to vulnerabilities within your application itself, including data leakage, remote code execution, etc.

- **You are responsible for backing up your data.** You are responsible for ensuring your critical data is backed up. We currently do not offer a backup service.
