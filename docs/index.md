# VT NI&S Common Application Platform

Network Infrastructure and Services provides a Kubernetes-based Common
Application Platform for use by NI&S and the broader University
community.  The documentation here is intended to assist teams with onboarding
their applications into the platform, and managing their applications once they
are onboarded.

## Why use the Common Application Platform?

Our goal is simply **"bring a container and we'll run it for you."** We want development teams to focus on what matters most to them... writing code to respond to the needs of their stakeholders and customers without worrying about infrastructure, networking, container orchestration, etc. At a high-level, we provide the following features:

- **Managed container-based hosting platform** - you bring a container and we'll run it! It doesn't matter what language or framework you use. Simply use what makes sense to you to meet your needs.
- **No more patching or managing machines** - we take care of the underlying infrastructure for you
- **Automatic log forwarding** - all applications automatically send logs to CLS, meeting requirements for log management defined in the [Standard for Information Technology Logging](https://it.vt.edu/content/dam/it_vt_edu/policies/Standard_for_Information_Technology_Logging.pdf)
- **Increased security posture** - all changes to the platform are performed through manifest repos or reviewed code processes. Even the admins on the platform team have read-only access and all state changes is logged.
- **CI assistance** - we've created [GitLab CI templates](https://code.vt.edu/it-common-platform/support/ci-templates) that support many of the common use cases for teams who are running builds on the platform or those that want to deploy on the platform
- **Standardized deployments** - by using the same methods of deployment used by other teams, our overall ability to share best practices and training increases


## What are the current limitations of the Common Application Platform?

- **Limited support** - since our team is still very limited in size, we are currently only providing business hour support. We will still do our best to respond to events during off-hours, but we simply can't provide a SLA around it
- **Linux-only support** - while Kubernetes supports Windows-based nodes, we currently do not have the expertise to support it... yet. However, we do support ARM-based images using Amazon's Graviton instances
