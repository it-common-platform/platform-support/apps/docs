# Onboarding

## Process

In order to onboard a new application, contact the product owner for the
platform, [Justin Strickland](mailto:jdstrick@vt.edu).

## Tenant Repository

As part of the onboarding process, a tenant subproject will be created for you 
in the
[tenants project](https://code.vt.edu/it-common-platform/tenants).
Within this repository you will be able to define resources which the GitOps
process (driven by Flux) will automatically deploy on the cluster.
