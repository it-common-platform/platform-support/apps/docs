# Getting Started

In this guide, you will complete the following objectives:

- Deploy an application on the platform, complete with an HTTPS endpoint
- Use the [Platform Dashboard](https://dashboard.platform.it.vt.edu) to view logs and pod details

## Deploying a Simple Application

There are several steps required to deploy an application and have it be accessible via HTTPS. Specifically, we need to define the application itself, expose it on the cluster network, request a TLS certificate, and define HTTP routing rules. While this sounds complicated, it's fairly simple. Let's get started!

### Defining the Application

In your tenant manifest repo, create a file named `deployment.yaml` with the following YAML. What this is doing is defining a simple application named `hello-world` which will use a container image we developed for the PilotFest workshop. This definition is also setting [resource requests and limits](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/), which is a good practice to help ensure that both your app gets the resources it needs and it doesn't affect others. We'll also see a [liveness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/), which tells us if the application is healthy and is useful when rolling out new updates.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world
spec:
  selector:
    matchLabels:
      app: hello-world
  template:
    metadata:
      labels:
        app: hello-world
    spec:
      containers:
        - name: hello-world
          image: code.vt.edu:5005/it-common-platform/tenant-support/images/pilotfest-2021/first-image:latest
          ports:
            - name: http
              containerPort: 3000
          resources:
            requests:
              memory: 32Mi
              cpu: 50m
            limits:
              memory: 128Mi
              cpu: 500m
          livenessProbe:
            httpGet:
              path: /
              port: 3000
```

Once you've committed this file, you should see the manifest be applied and a pod startup on the dashboard. Note that the pod name may not exactly match what you see in the sample screenshot.

<figure markdown>
  ![Pod card on the dashboard showing the newly deployed application](img/getting-started-deployment.png)
  <figcaption>Pod card on the dashboard showing the newly deployed application</figcaption>
</figure>


### Requesting a TLS Certificate

Now that we have a containerized application, let's start the process to expose it. The first thing we'll do is request a TLS certificate. For a domain name, all tenants are authorized to use `*.<tenant-id>.tenants.platform.it.vt.edu`. For this guide, we'll simply use the root of your tenant domain. But, you can create a subdomain if you'd like.

In your tenant repo, create a file named `certificate.yaml` with the following YAML. **Be sure to replace `my-tenant-name` in both `commonName` and `dnsNames` with your tenant identifier.** What this will do is request a TLS certificate for the specified names and store the private key and certificate into a Kubernetes secret named `hello-world-tls-cert`.

```yaml hl_lines="6 8"
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: hello-world
spec:
  commonName: my-tenant-name.tenants.platform.it.vt.edu
  dnsNames:
    - my-tenant-name.tenants.platform.it.vt.edu
  secretName: hello-world-tls-cert
  issuerRef:
    kind: ClusterIssuer
    name: letsencrypt
```

After defining the manifest, you'll probably see a few things happen on the dashboard. Since we are currently using LetsEncrypt to issue certificates, you will see a temporary pod and domain/ingress defined in your namespace to complete the HTTP challenge. Once the challenge is completed and the certificate is issued, you'll see it all disappear and you'll be left with a Certificate in the "Ready" state.

<figure markdown>
  ![Dashboard view of a Certificate in the ready state](img/getting-started-certificate.png)
  <figcaption>Dashboard view of a Certificate in the ready state</figcaption>
</figure>


### Defining the HTTP Routing Config

Now that we have a certificate defined, let's define our HTTP routing configuration. To keep it simple, we're going to send all requests for our domain to our application. The cluster routing components don't search for pods directly, but use "Services" to discover the application (more on that in the next step).

Create a file named `ingress.yaml` with the following YAML. This will define a routing rule that will send all requests to our tenant domain to a Service named `hello-world`, which we'll define in the next step. **Be sure to replace `my-tenant-name` in both the `host` and `tls.hosts` fields with your tenant identifier.** This also provides config to routing components to use the TLS key/cert stored in `hello-world-tls-cert` (which the Certificate defines and components will populate).

```yaml hl_lines="7 19"
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: hello-world
spec:
  rules:
    - host: my-tenant-name.tenants.platform.it.vt.edu
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: hello-world
              port: 
                number: 3000
  tls:
  - hosts:
      - my-tenant-name.tenants.platform.it.vt.edu
    secretName: hello-world-tls-cert  
```

Once this is defined, you should now see a listing in the "Your Domains" card, but no pods are connected to that ingress. As mentioned earlier, that's because we don't have the Service defined yet.

<figure markdown>
  ![Dashboard view of the domain card with a domain, but no connected pods](img/getting-started-ingress-no-connected-pods.png)
  <figcaption>Dashboard view of the domain card with a domain, but no connected pods</figcaption>
</figure>

In fact, if we click on the link for the domain, we can see more details about all of the connections between the routing config and the application. In this case, it reminds us that are missing the service

<figure markdown>
  ![Domain view showing the missing Service warning](img/getting-started-missing-service.png)
  <figcaption>Domain view showing the missing Service warning</figcaption>
</figure>


### Exposing the Application on the Cluster Network

Now, all we need to do is [define a Service](https://kubernetes.io/docs/concepts/services-networking/service/), which exposes the container on the cluster network. This will finally allow the pod to be discovered by the HTTP routing components.

In a file named `service.yaml`, place the following YAML to define the Service. What this is going to do is expose all pods that have a label with key `app` and value `hello-world` (which our Deployment defines).

```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-world
spec:
  selector:
    app: hello-world
  ports:
    - port: 3000
```

Once the manifest is applied, you should see the domain card be updated to indicate there is a pod that services the ingress. In addition, you'll see a "Open URL" link that allows you to quickly open the webpage. Open it up and you should see a simple "Todo App". Congrats! :tada:


## Using the Dashboard

Although we've used the dashboard a few times already, there are a few other neat capabilities that you might find useful.

### Viewing Pod Details and Logs

If you click on the link for a specific pod, you will see quite a few details about the pod, including environment variables, volume information, and resource utilization. In addition, there is a button on each container detail card that lets you see the container logs.

<figure markdown>
  ![Screenshot of the pod detail view, highlighting the "View Logs" button](img/getting-started-pod-detail-view.png)
  <figcaption>Screenshot of the pod detail view, highlighting the "View Logs" button</figcaption>
</figure>


### Using kubectl

While the dashboard provides a lot of insights, it doesn't expose everything. Kubectl is a Kubernetes command-line tool that can be configured to query resources, read logs, and more. 

1. If you don't already have kubectl (it's installed with Docker Desktop), [follow these instructions to install kubectl](https://kubernetes.io/docs/tasks/tools/).

1. After logging into the dashboard, click on the "Shell" button in the top nav menu. You'll then see a modal window open with a bunch of commands to run. Go ahead and copy/paste each of them and run them on your local machine.

1. Try to query the running pods and see if our hello world app is up and running:

    ```shell
    kubectl get pods
    ```

!!! note "Third-Party Integration and Support"
    The config is stored in `~/.kube/config` and is typically referred to as a "kubeconfig" file. Many other third-party tools can use this file to visualize and help you see what's going on with the cluster (for the resources you can see). You are welcome to explore them, but be mindful of procurement practices and who might have access to your config, as your credential is stored there.


## Next Steps

Now that you have a simple app up and running, it's time to replace it with your own app! To remove this sample app, simply remove the manifests.
