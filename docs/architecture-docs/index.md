# Architecture Documents

While building this platform, we have learned many ways to run things, as well as many ways _not_ to run a platform. To help others better understand how it works and to encourage discussion and advancement of the platform, we have created the following guides.

Note that these documents are quite technical and in-depth. The intended audience is not for those looking to run applications on the platform, but those desiring to understand how it works behind-the-scenes.

- [Achieving Multi-Tenancy](./achieving-multi-tenancy/) - provides a detailed overview on how the various design decisions and implementations necessary to isolate tenants from one another
- [DNS and Ingress Implementation](./dns-ingress-implementation/) - provides a detailed overview on some Kubernetes objects and design decisions wrt to DNS & Ingress to support applications running in the cluster
