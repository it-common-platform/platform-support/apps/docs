
# Guide Overview

The following guides are available to help you get going, based on common patterns and use cases we have seen over time. If you have ideas for more, feel free to let us know!

- [Assuming an AWS IAM Role](./assuming-an-aws-iam-role) - learn how to run your applications using an IAM role configured in your own AWS account, letting you access additional cloud resources
- [Pulling Images from a Private Registry](./pulling-images-from-private-registry) - learn how to create the necessary credentials to pull images from a private registry (minus ECR, which approaches credentialing in a different mannger)
- [Pulling Images from ECR](./pulling-images-from-ecr) - learn how to populate the necessary credentials needed to pull images from ECR and ensure those secrets are always up-to-date
- [Adding Storage to Apps](./adding-storage-to-apps/) - learn how to add both ephemeral and persistent storage to your applications using volumes
