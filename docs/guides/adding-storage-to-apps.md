# Adding Storage to Apps

When running applications, you might need additional storage for either persistence or scratch/work space. The Platform provides a few mechanisms to attach additional storage to your applications. You are also welcome to [assume an IAM role](./assuming-an-aws-iam-role.md) to use S3 buckets in your own account.


## Supported Storage Classes

The primary storage class for The Platform is EBS. It is already set as default in all tenant namespaces. The easiest way to use it is to omit the StorageClassName.

For posterity, the actual StorageClassName will depend on your node-pool with the pattern:

`ebs-{{ name }}-delete`

EFS is available upon request. Please contact support.

### Notes about EBS Volumes

A few specific notes about EBS volumes:

- EBS volumes are tied to a specific availability zone, so be mindful of how you deploy your application
- All volumes are automatically encrypted by default
- Volume resizing support is enabled. But note that AWS has a limit of one volume modification every 6 hours. Refer to the [EBS documentation](https://docs.aws.amazon.com/AWSEC2/latest/APIReference/API_ModifyVolume.html) for more details


### Notes about EFS Volumes

A few specific notes about EFS volumes:

- EFS volumes are available in all availability zones the Platform runs in
- The capacity/size of EFS is ignored, as EFS has no sizing and will grow/shrink as needed
- All data in transit is encrypted



## Using Ephemeral Storage

If you wish to add additional storage to your running pods, you can use the [Ephemeral Volume](https://kubernetes.io/docs/concepts/storage/ephemeral-volumes/) support built into Kubernetes. The following example will create a Pod that has a 1GB EBS volume and mounted at `/scratch`.

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: my-app
spec:
  containers:
    - name: app
      image: nginx:alpine
      volumeMounts:
      - mountPath: "/scratch"
        name: scratch-volume
  volumes:
    - name: scratch-volume
      ephemeral:
        volumeClaimTemplate:
          spec:
            accessModes: [ "ReadWriteOnce" ]
            resources:
              requests:
                storage: 1Gi
```

If used in a Deployment with multiple replicas, each Pod will get its own volume. When the pod is removed, the EBS volume is automatically removed.


## Persistent Storage

If you want your storage to live beyond the lifespan of a Pod, you need to create a `PersistentVolumeClaim` (often abbreviated as PVC). When this object is declared, the volume will be configured (but may not be provisioned until first use) and can then be referenced in your pod specification.

The following example will create a 4GB EBS volume.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: example-ebs-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 4Gi
```

Once you have the PVC defined, you can reference it in your Pod specification. The example below creates a Volume backed by the PVC and mounts it at `/data`. The container, while executing, will print out the current time to a file in `/data/out.txt`, storing it in the EBS volume.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: app
spec:
  containers:
    - name: app
      image: centos
      command: ["/bin/sh"]
      args: ["-c", "while true; do echo $(date -u) >> /data/out.txt; sleep 5; done"]
      volumeMounts:
        - name: persistent-storage
          mountPath: /data
  volumes:
    - name: persistent-storage
      persistentVolumeClaim:
        claimName: example-ebs-claim
```

