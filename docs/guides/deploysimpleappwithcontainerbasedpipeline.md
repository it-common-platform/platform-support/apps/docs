# Deploying a Simple App with a Container-centric Pipeline

## Tenant Repository

As part of the onboarding process, a tenant subproject will be created for you
in the
[tenants project](https://code.vt.edu/it-common-platform/tenants). This repository defines your app in terms of kubernetes resources.

 A great starting example to look at is the `it-common-platform-docs` project which hosts this information!

 Inside you will find a deployment which enumerates the single container used for the single declared service referencing a tagged image hosted in GitLab (This will be referenced/changed later):

 ``` yaml hl_lines="20"
apiVersion: apps/v1
kind: Deployment
metadata:
  name: docs
  namespace: it-common-platform-docs
  labels:
    app: docs
spec:
  replicas: 1
  selector:
    matchLabels:
      app: docs
  template:
    metadata:
      labels:
        app: docs
    spec:
      containers:
        - name: hello-world
          image: code.vt.edu:5005/it-common-platform/support/docs:master-d9ec5bc1-1630332292 # {"$imagepolicy": "it-common-platform-docs:docs"}
          imagePullPolicy: Always
          ports:
            - containerPort: 80
          resources:
            requests:
              memory: 64Mi
              cpu: 100m
            limits:
              memory: 128Mi
              cpu: 500m
  ```
  Next we create a simple service referencing the container created above by label and exposing port 80.
  ```yaml
  apiVersion: v1
kind: Service
metadata:
  name: docs-svc
  namespace: it-common-platform-docs
  labels:
    app: docs
spec:
  selector:
    app: docs
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  type: NodePort
```
The platform currently leverages ACME to request certificates from Let's Encrypt. That may expand in the future, but the request syntax should be almost identical.
```yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: docs-cert
  namespace: it-common-platform-docs
spec:
  commonName: docs.platform.it.vt.edu
  dnsNames:
    - docs.prod.clusters.aws.platform.it.cloud.vt.edu
    - docs.platform.it.vt.edu
  secretName: docs-cert
  issuerRef:
    kind: ClusterIssuer
    name: letsencrypt
```
Finally, with a deployment exposed by a service and a certificate held as a kubernetes secret from Let's Encrypt, it can all be referenced in an ingress referencing those pieces.
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: docs-ingress
  namespace: it-common-platform-docs
  labels:
    app: docs
spec:
  rules:
    - host: "docs.prod.clusters.aws.platform.it.cloud.vt.edu"
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: docs-svc
              port:
                number: 80
    - host: "docs.platform.it.vt.edu"
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: docs-svc
              port:
                number: 80
  tls:
  - hosts:
    - "docs.prod.clusters.aws.platform.it.cloud.vt.edu"
    - docs.platform.it.vt.edu
    secretName: docs-cert
```
In order to leverage flux.cd to do our pipeline and deployments, we need to provide it some information. Below you see broken out sections for this guide. I have done this and omitted the `---` separating each section in the original config.

In order to provide access to GitLab, we create an access secret. (You will notice that these are seal secrets, please see [here](https://github.com/bitnami-labs/sealed-secrets) for more information)
```yaml
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  creationTimestamp: null
  name: flux-image-https-credential
  namespace: it-common-platform-docs
spec:
  encryptedData:
    password:
    username:
  template:
    data: null
    metadata:
      creationTimestamp: null
      name: flux-image-https-credential
      namespace: it-common-platform-docs
```
We are currently on flux v2 which allows the use of webhooks to trigger flux operation instead of polling, this token dictates that trigger.
```yaml
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  creationTimestamp: null
  name: flux-image-webhook-token
  namespace: it-common-platform-docs
spec:
  encryptedData:
    token:
  template:
    data: null
    metadata:
      creationTimestamp: null
      name: flux-image-webhook-token
      namespace: it-common-platform-docs
```
We create a repository for Flux to watch, matching it with the secret holding the credential. Note this is the tenant, not the app repository. Manipulating this repository will allow flux to change the image listed in our deployment.
```yaml
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: docs-repo
spec:
  interval: 6h # we're not using to sync manifests since we are syncing on images, so no need to pull often
  url: https://code.vt.edu/it-common-platform/tenants/aws-prod/it-common-platform-docs.git
  secretRef:
    name: flux-image-https-credential
  ref:
    branch: master
```
Next we give flux an image repository to watch for new images created by our pipeline we will define in the App Poject.
```yaml
apiVersion: image.toolkit.fluxcd.io/v1beta2
kind: ImageRepository
metadata:
  name: docs
spec:
  image: code.vt.edu:5005/it-common-platform/support/docs
  interval: 2h # long interval because we'll use webhooks
```
We need to tell flux which images are candidates for release and out of the list how to select them.
```yaml
apiVersion: image.toolkit.fluxcd.io/v1beta2
kind: ImagePolicy
metadata:
  name: docs
spec:
  imageRepositoryRef:
    name: docs
  filterTags:
    # Images are tagged as <branch>-<short-sha>-<build-time>
    pattern: '^master-[a-fA-F0-9]+-(?P<ts>.*)'
    extract: '$ts'
  policy:
    numerical:
      order: asc
```
This gives flux information on how to apply the new image to the tenant repo's via git.
```yaml
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImageUpdateAutomation
metadata:
  name: docs
spec:
  interval: 2h
  sourceRef:
    kind: GitRepository
    name: docs-repo
  git:
    checkout:
      ref:
        branch: master
    commit:
      author:
        email: fluxcdbot@code.vt.edu
        name: fluxcdbot
      messageTemplate: '{{range .Updated.Images}}{{println .}}{{end}}'
    push:
      branch: master
  update:
    path: /
    strategy: Setters
```
This uses the token created above and coordinates it with flux v2.
```yaml
apiVersion: notification.toolkit.fluxcd.io/v1beta2
kind: Receiver
metadata:
  name: docs-image
spec:
  type: generic
  secretRef:
    name: flux-image-webhook-token
  resources:
    - apiVersion: image.toolkit.fluxcd.io/v1beta1
      kind: ImageRepository
      name: docs
      namespace: it-common-platform-docs
```

For more information about the image pipeline, continue through `App Project` below.

## App Project
You will want to create an app project to hold your app implementation. Again, a great example is the [docs](https://code.vt.edu/it-common-platform/support/docs) project that is used to provide this information!

Inside you will find a dockerfile which builds the app into a docker image, a gitlab ci pipeline configuration, an example docker-compose for local testing, and resources necessary for building the image.

Inside the .gitlab-ci.yml in this particular project, we have linked to another helpful project, [CI Templates](https://code.vt.edu/it-common-platform/support/ci-templates), which we recommend looking through to assist with creation of any pipelines you may want. In summary, when a commit is made on the pipeline's default branch, it triggers a build and then later, the webhook to have flux run, changing the deployment above.
```yaml
stages:
  - build
  - deploy

include:
  project: it-common-platform/support/ci-templates
  file: building-blocks/docker-build.yml

Build image:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  extends: .docker-build

Trigger webhook:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $IMAGE_WEBHOOK_URL
  image:
    name: curlimages/curl:latest
    entrypoint: [""]
  script:
    - curl -L $IMAGE_WEBHOOK_URL
```
In the case of docs, the pipeline leverages a preconfigured and variablized docker-build which builds your app using the instructions in the dockerfile then deploys it to the image repo. If the commit happens on the default branch, the project is updated and the pipeline pushes the image to prod via tags, leveraging the swiftness of Flux v2's webhook method.

### Utilizing the CI/CD Pipeline

Beyond the forementioned setup, this method will require a [git-lab runner](https://docs.gitlab.com/runner/)  and the webhook url to be saved in [GitLab variables](https://docs.gitlab.com/12.10/ee/ci/variables/).

After those are setup, any commits on the default branch will be deployed to the platform shortly using flux which is already setup on the platform using the configuration we provided in the tenant repository.


!!! note "For easy testing and migration"
    In order to foster easy implementation, we provide tennant specific hostnames you can use to test with and CNAME to in order to test your app without switching DNS over to it. Every tennant gets assigned `<tenant-name>.tenants.platform.it.vt.edu` allowing you to test your application using all the amnenities of the cluster.


