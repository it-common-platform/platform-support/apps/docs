# Pulling Images from a Private Registry

In order to pull container images from a private registry, the container runtime needs credentials. Simply put, we need to do the following:

1. Define a Kubernetes secret that contains the auth credential
1. Update our Pod/Deployment specs to reference the Kubernetes secret

Let's get to it!


## Defining the Registry Secret

While you could simply define the Kuberentes secret, we _strongly_ recommend you not to do that as your secret would be sitting in your manifest repo unencrypted. Therefore, we support two methods to populate the Kubernetes secrets:

- **Sync from Vault** - if your team is using Vault for secrets management, you can use Vault as the source of truth
- **Using Sealed Secrets** - we can create a manifest with encrypted secret details, which is decrypted and converted into a Kubernetes secret

When do you want to use which approach? Basically, if you're using Vault to store your secrets, sync from Vault. Otherwise, use the Sealed Secrets approach.


### Sync from Vault

To sync secrets from Vault, there are two steps needed:

1. **Vault configuration** - we need to create a Vault role and policy that grants access to an application running in the cluster to access the desired secret(s)
1. **Kubernetes Job/CronJob** - we need to run a task in the cluster that will use the configured Vault role to fetch the secret and create the corresponding Kubernetes secret

#### Vault Configuration

As of right now, all Vault config is done using Terraform in the [es/Vault/config](https://code.vt.edu/es/Vault/config) repo on code.vt.edu. To grant access to applications running in the platform, we've created a Terraform module to minimize the amount of config you need to write. 

1. After cloning the repo locally, create a file named `app-<org>-<app-name>.tf` in the root directory.
    - Replace `<org>` with your unit's name (eg, `dit-es`, `dit-middleware`, `dit-platform`) 
    - Replace `<app-name>` with the name of the app you're working with (e.g., `hello-world`)

1. In that newly created file, place the following Terraform config:

    ```hcl
    module "app_<org>_<app_name>" {
      source = "./modules/platform-access"

      app_name             = "<org>-<app-name>"
      tenant_identifier    = "<your-tenant-identifier>"
      service_account_name = "vault-secret-syncer"
      secret_paths         = ["path/to/your/secret"]
    }
    ```

    - Replace `<org>_<app_name>` with the same values you used in the filename, just with underscores to separate words (per TF naming conventions)
    - Replace `<org>-<app-name>` with the same values you used in the filename
    - Replace `<your-tenant-identifier>` with the name of your platform tenant, which is the same as the cluster namespace
    - Replace `path/to/your/secret` with the path to your secret. **Important note:** the second segment should be `data`. If I were to use the Vault UI and navigate to a secret using the path `dit.platform/demo/registry-credential`, I would set the path to `dit.platform/data/demo/registry-credential`

    Example config:

    ```hcl
    module "app_dit_platform_hello_world" {
      source = "./modules/platform-access"

      app_name             = "dit-platform-hello-world"
      tenant_identifier    = "it-common-platform-hello-world"
      service_account_name = "vault-secret-syncer"
      secret_paths         = ["dit.platform/data/demo/registry-credential"]
    }
    ```

1. Commit the change on a new branch, push the branch, and create a merge request. The Vault admin team will review it and it will be merged when approved.


#### Defining the Kubernetes Job/CronJob

Now, we need to define a task to create the Kubernetes secret. To simplify this process, we [have made a Helm chart](https://code.vt.edu/it-common-platform/support/helm-charts/vault-secret-syncer/) that will create the necessary components!

1. In your platform manifest repo, create a file named `vault-secret-syncer.yml`. In that file, place the following:

    ```yaml
    apiVersion: helm.toolkit.fluxcd.io/v2beta1
    kind: HelmRelease
    metadata:
      name: vault-secret-syncer
    spec:
      interval: 1h
      targetNamespace: <tenant-identifier>
      serviceAccountName: flux
      chart:
        spec:
          chart: vault-secret-syncer
          sourceRef:
            kind: HelmRepository
            name: vault-secret-syncer
            namespace: platform-helm-repos
      values:
        vault:
          role: it-platform-<module.app_name>
          secret:
            path: <path/to/registry/credential>
            usernameKey: <usernameKey>
            passwordKey: <passwordKey>
        # secret:
        #   registryUrl: code.vt.edu:5005
    ```

    - Replace `<tenant-identifier>` with your tenant identifier (or namespace)
    - Replace `<module.app_name>` with the same value you put in the Vault Terraform config in the `app_name` field
    - Replace `<path/to/registry/credential>` with the path to the registry credential. **Important note:** the second segment in the path should be `data`, just as before
    - Replace `<usernameKey>` with the key in the Vault secret that contains the registry auth username
    - Replace `<passwordKey>` with the key in the Vault secret that contains the registry auth password
    - If you are pulling secrets from another registry, uncomment the two lines and adjust the registryUrl (e.g., `code.vt.edu:5005` for GitLab)
    - Any other possible configuration for the chart is [documented in the chart's values.yaml](https://code.vt.edu/it-common-platform/support/helm-charts/vault-secret-syncer/-/blob/main/values.yaml).

1. Commit the manifest and push it to your manifest repo. After a moment, you should see a Pod startup that runs and creates the secret!


### Using Sealed Secrets

The [Platform Dashboard](https://dashboard.platform.it.vt.edu) provides a tool to create "Sealed Secrets". These secrets are encrypted using a public key and can only be decrypted using a private key held within the cluster. This provides the ability to commit "secrets" in a git repo without doing so unsafely.

To create a Sealed Secret to be used to pull credentials, you can:

1. Open the [Platform Dashboard](https://dashboard.platform.it.vt.edu) and navigate to the namespace the secret will live in. This is important as the secret can only be decrypted in the namespace it was encrypted for.

1. Click on the **Sealed Secrets Tool** in the top navigation menu.

1. Specify a _Secret Name_.

1. In _Secret Type_, change the option to **Image Registry**.

1. For the _Registry URL_, enter the domain name of the registry (e.g., code.vt.edu:5005)

1. Enter the _Username_ and _Password_. For GitLab, this can be a [project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) or [Deploy Key](https://docs.gitlab.com/ee/user/project/deploy_tokens/).

    ![Sample Sealed Secrets screenshot](/img/sealed-secrets-registry-screenshot.png)

1. Click **Generate Manifest** and add the manifest to your manifest repo. Once synced, the SealedSecret will be decrypted and stored as a normal Kubernetes secret.


## Updating the Pod/Deployment Spec

Once the secrets are defined, all we have to do is update our pod specification to reference the secret name. All you have to define is the `imagePullSecrets` field.

!!! note

    If using the Helm chart to sync secrets from Vault, the default secret name is named `registry-auth`. You can override it by specifying the `secret.name` field in the HelmRelease `values`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: example-pod
spec:
  imagePullSecrets:
    - name: registry-auth
  containers:
    - name: app
      image: code.vt.edu:5005/sample/image
```

Once you commit this change to your manifest repo, you should now see your image get pulled and start running! That's all there is to it!
