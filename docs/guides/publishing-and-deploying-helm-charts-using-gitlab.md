# Publishing and Deploying Helm Charts using GitLab

Helm charts provide a powerful ability to template manifests, making it easy to deploy multiple instances of an application (e.g., to support feature-branch deployments).

This guide isn't going to talk about how to actually write a Helm chart, as there are [several](https://helm.sh/docs/chart_template_guide/) [other](https://docs.bitnami.com/tutorials/create-your-first-helm-chart/) [guides](https://opensource.com/article/20/5/helm-charts) available to do this.


## Publishing your Chart in GitLab

To publish a chart in GitLab, you can leverage a CI template that we've built. In your repo, create a `.gitlab-ci.yml` file with the following contents:

```yaml
include:
  project: it-common-platform/tenant-support/ci-templates
  file: building-blocks/helm.yml

stages:
  - deploy

Deploy chart:
  extends: .helm-publish
  rules:
    - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - Chart.yaml
```

This will extend one of our "building blocks" and deploy the chart anytime the `Chart.yaml` file is updated.

### Deploying a chart not at the root of your repo

If you need to deploy a chart that's nested in your repo, you can set the `HELM_CHART_LOCATION`. This value defaults to the root of your repo.

```yaml
Deploy chart:
  extends: .helm-publish
  variables:
    HELM_CHART_LOCATION: chart/path
```


## Deploying your Chart on the Platform

Once you have a chart published, you can deploy it using [Flux's Helm Release support](https://fluxcd.io/docs/guides/helmreleases/). With Flux, we need to define a source (the `HelmRepository`) and a reconciler (the `HelmRelease` which actually defines how and where the chart should be deployed).

1. If your chart is part of a private or internal project in GitLab, you will need to provide Flux with credentials to fetch the chart.

    1. Go to **Settings** -> **Repository** -> **Deploy tokens** and generate a new deploy token. You can give it any name, but it _must_ have the **read_package_registry** permission.

    1. Open the [Platform Dashboard](https://dashboard.platform.it.vt.edu) and navigate to the namespace you are going to deploy the chart into.

    1. Open the "Sealed Secrets Tool", accessible using the link in the top navbar.

    1. Create a secret named `helm-chart-credential`. Create one item with a key of **username** and a value of the deploy token username. Create another item with a key of **password** with the deploy token token.

    1. Click "Generate Manifest" and copy the `SealedSecret` manifest into your manifest repo.

1. Now, in your manifest repo, you can define your `HelmRepository`. You will need your GitLab project ID, which is accessible on the project's home page. Swap that into the URL below

    ```yaml
    apiVersion: source.toolkit.fluxcd.io/v1beta1
    kind: HelmRepository
    metadata:
      name: gitlab
    spec:
      url: https://code.vt.edu/api/v4/projects/<gitlab-project-id>/packages/helm/stable
      interval: 5m
      secretRef: 
        name: helm-chart-credential
    ```

1. When you add that, you can query using kubectl to see the object (it's not yet visible using the dashboard).

    ```shell
    kubectl get helmrepositories -n <your-tenant-namespace>
    ```

    And you'll see output similar to the following:

    ```plaintext
    NAME       URL                                                             READY   STATUS                                                                               AGE
    landlord   https://code.vt.edu/api/v4/projects/8107/packages/helm/stable   True    Fetched revision: e149da53ec05d971e5400843c8f78f427476985f490b840c7ca00c8bdfc00204   36d
    ```

1. From there, you can define a `HelmRelease` that will deploy the chart into your tenant namespace!

    ```yaml
    apiVersion: helm.toolkit.fluxcd.io/v2beta1
    kind: HelmRelease
    metadata:
      name: landlord
    spec:
      chart:
        spec:
          interval: 1h0m0s
          sourceRef:
            kind: HelmRepository
            name: landlord
          chart: landlord
          version: 0.20.0
      interval: 5m0s
      releaseName: landlord
      # values:
      #   key: any custom values you want to override from the chart 
    ```

1. Once defined, you can query the `HelmRelease` using kubectl. Currently, we have two versions of HelmReleases defined, so we have to be a little more explicit in which version we want to fetch.

    ```shell
    kubectl get helmreleases.helm.toolkit.fluxcd.io -n <your-tenant-namespace>
    ```

    And you'll see output like the following:

    ```plaintext
    NAME       READY   STATUS                             AGE
    landlord   True    Release reconciliation succeeded   36d
    ```

## Additional Resources

The Flux docs have quite a few guides on using Helm charts. Links are available below:

- [Managing Helm Releases](https://fluxcd.io/docs/guides/helmreleases/)
- [Flux for Helm Users](https://fluxcd.io/docs/use-cases/helm/)

