# Assuming an AWS IAM Role

You may find your app requires additional resources that are not available inside the cluster. By deploying them in your own AWS account, you can quickly iterate and add resources without needing to wait for platform support. The platform makes it easy for you to access your resources in a safe and secure manner.

## AWS Roles

In AWS, IAM roles are an identity that has specific permissions. But, instead of the permissions being granted to a specific person, they are granted to an identity, which can be people, applications, or machines. The credentials are temporary, preventing the chance of long-lived tokens being leaked.

In order to access your resources, you will need to create an IAM role with the permissions your application needs.


## Accessing your Cloud Resources from the Platform

We have created and maintain [a Terraform module](https://code.vt.edu/it-common-platform/tenant-support/terraform/aws-platform-role-connector) that does a lot of the heavy lifting for you. 

The example code below will uses this module to create a role in your account, add the platform EKS cluster as a valid identity provider for the role, and allow access from an application in your Kubernetes namespace to use the role.

```ini
module "plaform-role-connector" {
  source                   = "git@code.vt.edu:it-common-platform/tenant-support/terraform/aws-platform-role-connector.git"
  role_name                = "cluster-app-role"
  role_description         = "A role that can be assumed by the Common Platform"
  k8s_namespace            = "examplenamespace"
  k8s_service_account_name = "exampleserviceaccountname"
}
```

## Using your shared resource in Platform's EKS cluster

Once you have your AWS role created and configured, we only need to setup your application to use the role! Due to automation in the cluster, we only have to create a ServiceAccount and tell your application to use it. From there, your application will start with the tokens that are needed to assume a role in your account.

### Creating a ServiceAccount to Assume a Role

The snippet below will create a ServiceAccount. Note the annotation on it, which specifies the ARN of the AWS role you have configured.

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: exampleserviceaccountname
  annotations:
    eks.amazonaws.com/role-arn: arn:aws:iam::<accountnumber>:role/<AWS Role name>
```

### Using the ServiceAccount in your Application

Once you have the ServiceAccount created, you only need to specify the ServiceAccount. If you're deploying a Pod directly, you can use the following snippet:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  serviceAccountName: exampleserviceaccountname
  containers:
    ...
```

If you're defining a Deployment, you'd do the same thing. But, the `serviceAccountName` is nested a little deeper.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-pod
spec:
  selector:
    matchLabels:
      app: my-pod
  template:
    metadata:
      labels:
        app: my-pod
    spec:
      serviceAccountName: exampleserviceaccountname
      containers:
        ...
```

## Learn More!

If you're interested in learning how the role assumption works, you can use the following resources:

- [EKS Pod Identity Webhook Deep-Dive](https://blog.mikesir87.io/2020/09/eks-pod-identity-webhook-deep-dive/)
