# IMPORTANT
This repo is superceded by the [S4 repo.](https://code.vt.edu/s4-hosting-sites/s4-docs) and has therefore been archived.

# Platform User Docs

This repo provides the documentation for the Common Application Platform.

## Developing/Contributing

If you'd like to contribute/modify the docs, you can use the provided `docker-compose.yml` file. Simply run

```
docker compose up -d
```

Once it's up and running, open your browser to [https://docs.localhost.devcom.vt.edu](https://docs.localhost.devcom.vt.edu). As you make changes to the files, they will automatically be refreshed in the browser. Note that changes to the `mkdocs.yml` file may require a restart as that file is typically only read at startup.
