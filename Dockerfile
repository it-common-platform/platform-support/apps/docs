FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/squidfunk/mkdocs-material:latest AS base
RUN pip install mkdocs-macros-plugin

FROM base AS build
ARG CLUSTER=builddefault
ENV CLUSTER=${CLUSTER}
ADD . /docs
RUN mkdocs build

FROM nginx:stable-alpine
COPY --from=build /docs/site/ /usr/share/nginx/html/
