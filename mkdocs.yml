site_name: Common Platform Docs
site_url: https://docs.prod.clusters.aws.platform.it.cloud.vt.edu/
repo_url: https://code.vt.edu/it-common-platform/platform-support/apps/docs
edit_uri: -/edit/master/docs
repo_name: Source repo
theme:
  name: material
  features:
    - navigation.expand
  icon:
    repo: fontawesome/brands/gitlab
  font:
    text: Acherus
    code: IBM Plex Mono

plugins:
  - macros

extra_css:
  - https://assets.cms.vt.edu/fonts/fonts.css
  - stylesheets/extra.css

extra:
  url:
    domain: clusters.aws.platform.it.cloud.vt.edu
    cluster: !ENV [CLUSTER, 'CLUSTER']

nav:
  - Home: index.md
  - Core Concepts:
    - Core Tenets: core-concepts/core-tenets.md
    - The 5000ft View: core-concepts/platform-high-level.md
    - Separation of Concerns: core-concepts/separation-of-concerns.md
    - Kubernetes Basics: core-concepts/kubernetes-basics.md
  - Getting Started: getting-started.md
  - Guides:
    - Overview: guides/index.md
    - Assuming an AWS IAM Role: guides/assuming-an-aws-iam-role.md
    - Pulling Images from a Private Registry: guides/pulling-images-from-private-registry.md
    - Pulling Images from ECR: guides/pulling-images-from-ecr.md
    - Adding Storage to Apps: guides/adding-storage-to-apps.md
    - Publishing and Deploying Helm Charts using GitLab: guides/publishing-and-deploying-helm-charts-using-gitlab.md
  - Architecture Docs:
    - Overview: architecture-docs/index.md
    - Achieving Multi-tenancy: architecture-docs/achieving-multi-tenancy/index.md
    - DNS and Ingress Implementation: architecture-docs/dns-ingress-implementation/index.md

markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.highlight
  - pymdownx.superfences
  - md_in_html
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
